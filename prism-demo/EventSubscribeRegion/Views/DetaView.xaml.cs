﻿using Prism.Common;
using Prism.EventSubscribeModule.ViewModels;
using Prism.Regions;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prism.EventSubscribeModule.Views
{
    /// <summary>
    /// DetaView.xaml 的交互逻辑
    /// </summary>
    public partial class DetaView : UserControl
    {
        public DetaView()
        {
            InitializeComponent();
            RegionContext.GetObservableContext(this).PropertyChanged += (sender, arg) =>
            {
                var content = sender as ObservableObject<object>;//通知
                //var vm = DataContext as DetaViewModel;
                //vm.Message = content.Value as MessageModel;
                (DataContext as DetaViewModel).Message = content.Value as MessageModel;
            };
        }
    }
}
