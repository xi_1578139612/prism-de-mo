﻿using Prism.EventSubscribeModule.Views;
using Prism.EventSubscribeRModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.EventSubscribeModule
{
    public class EventSubscribeModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<SubscribeView>(RegionNames.EventSubscribeRegion);
            containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<DetaView>(RegionNames.DetaRegion);
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            
        }
    }
}
