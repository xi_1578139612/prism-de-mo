﻿using Prism.Mvvm;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.EventSubscribeModule.ViewModels
{
    public class DetaViewModel : BindableBase
    {
        private MessageModel message;

        public MessageModel Message
        {
            get { return message; }
            set { message = value; RaisePropertyChanged(); }
        }

        //private DateTime time;

        //public DateTime Time
        //{
        //    get { return time; }
        //    set { time = value; RaisePropertyChanged(); }
        //}
    }
}
