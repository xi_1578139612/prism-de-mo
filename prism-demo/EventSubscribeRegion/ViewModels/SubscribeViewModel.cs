﻿using Prism.Events;
using Prism.Mvvm;
using Prism.Share.Event;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.EventSubscribeRModule.ViewModels
{
    public class SubscribeViewModel : BindableBase
    {
        IEventAggregator eventAggregator;
        public SubscribeViewModel(IEventAggregator eventAggregator )
        {
             this.eventAggregator = eventAggregator;
            eventAggregator.GetEvent<MessageEvent>().Subscribe((model) =>
            {
                MessageList.Add(model);
            });

            //过滤器
            eventAggregator.GetEvent<MessageEvent>().Subscribe((model) =>
            {
                MessageList.Add(model);
            },ThreadOption.PublisherThread,false,(model)=> model.Message.Contains("1"));
        }

        private ObservableCollection<MessageModel> messageModels = new ObservableCollection<MessageModel>() ;

        public ObservableCollection<MessageModel> MessageList
        {
            get { return messageModels; }
            set { messageModels = value; RaisePropertyChanged(); }
        }



    }
}
