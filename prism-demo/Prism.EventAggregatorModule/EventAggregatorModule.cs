﻿using Prism.EventAggregatorModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.EventAggregatorModule
{
    public class EventAggregatorModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<EventAggregatorView>(RegionNames.EventRegion);
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
