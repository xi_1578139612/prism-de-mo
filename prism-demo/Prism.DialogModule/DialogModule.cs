﻿using Prism.DialogModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.DialogModule
{
    public class DialogModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<DialogView>(RegionNames.DialogRegion);
            
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
