﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Share.Event;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.EventPublishModule.ViewModels
{
    public class PublishViewModel : BindableBase
    {
        IEventAggregator eventAggregator;
        public PublishViewModel(IEventAggregator eventAggregator) 
        { 
            this.eventAggregator = eventAggregator;
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; RaisePropertyChanged(); }
        }

        public DelegateCommand SendCommand => new DelegateCommand(() =>
        {
            if (!string.IsNullOrEmpty(message))
            {
                MessageModel model = new MessageModel() {Message=message ,Time = DateTime.Now };
                eventAggregator.GetEvent<MessageEvent>().Publish(model);
                Message = string.Empty;
            }
        });

    }
}
