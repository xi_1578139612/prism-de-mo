﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.NavigationModule.Views;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.NavigationModule
{
    public class NavigationModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<NavigationView>(RegionNames.NavigationRegion);
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<RegistryAView>();
            containerRegistry.RegisterForNavigation<RegistryBView>();
            containerRegistry.RegisterForNavigation<RegistryCView>();
            containerRegistry.RegisterForNavigation<RegistryOKView>();
        }
    }
}
