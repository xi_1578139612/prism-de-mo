﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.NavigationModule.Views;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.NavigationModule.ViewModels
{
    public class NavigationViewModel : BindableBase
    {
        private IRegionManager regionManager;
        public NavigationViewModel(IRegionManager regionManager)
        {
            //regionManager.RegisterViewWithRegion<RegistryAView>(RegionNames.RegistryRegion);//没有日志导航的入口
            regionManager.RegisterViewWithRegion<RegistryAView>(RegionNames.JournalRegion);//拥有日志导航的入口
            this.regionManager = regionManager;
        }
        public string Title => "Navigation/导航";

        private IRegionNavigationJournal journal;

        public IRegionNavigationJournal Journal
        {
            get { return journal; }
            set { journal = value; }
        }

        public DelegateCommand NavigationgNameCommand => new DelegateCommand(() =>
        {
            regionManager.RequestNavigate(RegionNames.JournalRegion, nameof(RegistryAView),navigationCallback);
        });

        public DelegateCommand NavigationgPsaawordCommand => new DelegateCommand(() =>
        {
            regionManager.RequestNavigate(RegionNames.JournalRegion, nameof(RegistryBView), navigationCallback);
        });

        public DelegateCommand NavigationgNoteCommand => new DelegateCommand(() =>
        {
            regionManager.RequestNavigate(RegionNames.JournalRegion, nameof(RegistryCView), navigationCallback);

        });

        

        private void navigationCallback(NavigationResult result)
        {
            Journal = result.Context.NavigationService.Journal;
        }

        public DelegateCommand GoBackCommand => new DelegateCommand(() =>
        {
            Journal.GoBack();
        });

        public DelegateCommand GoFoewordCommand => new DelegateCommand(() =>
        {
            Journal.GoForward();
        });
    }
}
