﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.NavigationModule.Views;
using Prism.Regions;
using Prism.Share;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.NavigationModule.ViewModels
{
    public class RegistryOKViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        public RegistryOKViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }
        private Person person = new Person();

        public Person Person
        {
            get { return person; }
            set { person = value; RaisePropertyChanged(); }
        }

        public DelegateCommand NextCommand => new DelegateCommand(() =>
        {
            _regionManager.RequestNavigate(RegionNames.RegistryRegion, nameof(RegistryAView));
        });
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //进来时候
            var keys = navigationContext.Parameters;
            var preson = keys.GetValue<Person>("person");
            this.Person = preson;
        }
    }
}
