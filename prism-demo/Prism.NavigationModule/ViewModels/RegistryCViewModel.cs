﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.NavigationModule.Views;
using Prism.Regions;
using Prism.Share;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Prism.NavigationModule.ViewModels
{
    public class RegistryCViewModel : BindableBase, IConfirmNavigationRequest
    {
        private IRegionManager _regionManager;
        public RegistryCViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }
        private Person person = new Person();

        public Person Person
        {
            get { return person; }
            set { person = value; RaisePropertyChanged(); }
        }

        public DelegateCommand NextCommand => new DelegateCommand(() =>
        {
            //参数传递(把用户名输入到b页面)
            NavigationParameters keys = new NavigationParameters();
            keys.Add("person", person);

            _regionManager.RequestNavigate(RegionNames.JournalRegion, nameof(RegistryOKView), keys);
        });
        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //进来时候
            var keys = navigationContext.Parameters;
            var preson = keys.GetValue<Person>("person");
            this.Person = preson;
        }

        void IConfirmNavigationRequest.ConfirmNavigationRequest(NavigationContext navigationContext, Action<bool> continuationCallback)
        {
            continuationCallback(true);
            return;
            //确认注册
            if (MessageBox.Show("是否注册？","提示",MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                continuationCallback(true);
            }
            else
            {
                continuationCallback(false);
            }
        }
    }
}
