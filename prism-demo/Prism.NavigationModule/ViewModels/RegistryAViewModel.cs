﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.NavigationModule.Views;
using Prism.Regions;
using Prism.Share;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.NavigationModule.ViewModels
{
    public class RegistryAViewModel : BindableBase, INavigationAware
    {
        private IRegionManager _regionManager;
        public RegistryAViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }
        private Person person = new Person();

        public Person Person
        {
            get { return person; }
            set { person = value;RaisePropertyChanged(); }
        }

        public DelegateCommand NextCommand => new DelegateCommand(() =>
        {
            //参数传递(把用户名输入到b页面)
           
            NavigationParameters keys = new NavigationParameters();
            keys.Add("person", person);

            _regionManager.RequestNavigate(RegionNames.JournalRegion,nameof(RegistryBView),keys);
        });

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
           
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            Person = new Person();
        }
    }
}
