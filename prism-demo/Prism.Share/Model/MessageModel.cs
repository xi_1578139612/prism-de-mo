﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.Share.Model
{
    public class MessageModel : BindableBase
    {
		private string message;

		public string Message
		{
			get { return message; }
			set { message = value; RaisePropertyChanged(); }
		}

		private DateTime time;

		public DateTime Time
		{
			get { return time; }
			set { time = value;RaisePropertyChanged(); }
		}


	}
}
