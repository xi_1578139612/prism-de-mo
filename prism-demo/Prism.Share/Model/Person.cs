﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.Share.Model
{
    public class Person : BindableBase
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged(); }
        }

        private string password;

        public string PassWord
        {
            get { return password; }
            set { password = value; RaisePropertyChanged(); }
        }

        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; RaisePropertyChanged(); }
        }
    }
}
