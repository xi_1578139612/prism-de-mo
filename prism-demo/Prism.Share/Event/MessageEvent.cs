﻿using Prism.Events;
using Prism.Share.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.Share.Event
{
    public class MessageEvent : PubSubEvent<MessageModel>
    {
    }
}
