﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.Share
{
    public class RegionNames
    {
        public static string HeaderRegion => "HeaderRegion";

        public static string AdapterRegion => "AdapterRegion";
        public static string DialogRegion => "DialogRegion";
        public static string EventRegion => "EventRegion";

        public static string NavigationRegion => "NavigationRegion";
        public static string ItemControlRetion => "ItemControlRetion";
        public static string TabControlRetion => "TabControlRetion";
        public static string StankPanelRetion => "StankPanelRetion";

        public static string EventPublishRegion => "EventPublishRegion";
        public static string EventSubscribeRegion => "EventSubscribeRegion";
        public const string DetaRegion = "DetaRegion";
        public const string RegistryRegion = "RegistryRegion";
        public const string JournalRegion = "JournalRegion";
        
    }
}
