﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using PrismDemo.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace PrismDemo.ViewModels
{
    internal class MainWindowViewModel:BindableBase
    {
        public string Title => "prism课程";

        public MainWindowViewModel(IRegionManager regionManager)
        {
            regionManager.RegisterViewWithRegion("HeaderRegion", "HeaderView");
        }
    }


}
