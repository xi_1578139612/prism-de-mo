﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PrismDemo.ViewModels
{
    public class HeaderViewModel : BindableBase
    {
        public string Title => "prism课程";

        //public ICommand GetCommand;

        //public MainWindowViewModel()
        //{
        //    GetCommand = new DelegateCommand(get);
        //}

        //private void get()
        //{
        //    System.Diagnostics.Process.Start("http://www.baidu.com/");

        //}

        private String getime;

        public String GetTime
        {
            get { return getime; }
            set { getime = value; RaisePropertyChanged(); }
        }

        private String keyword;

        public String KeyWord
        {
            get { return keyword; }
            set { keyword = value; RaisePropertyChanged(); }
        }



        public DelegateCommand GetCommand => new DelegateCommand(() =>
        {
            System.Diagnostics.Process.Start("http://www.baidu.com/");
            GetTime = DateTime.Now.ToString();
        });

        public DelegateCommand TextBlockMouseUpCommand => new DelegateCommand(() =>
        {
            MessageBox.Show("你单机了我");
        });

        //带参数的命令
        public DelegateCommand<TextBlock> TextBlockParamerCommand => new DelegateCommand<TextBlock>((textblock) =>
        {
            MessageBox.Show($"{textblock.Text}");
        });

        //搜索
        public DelegateCommand SearchCommand => new DelegateCommand(() =>
        {
            MessageBox.Show($"你要搜索的关键词：{KeyWord}");
        });
    }
}
