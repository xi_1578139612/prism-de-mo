﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.AdapterModule.ViewModels
{
    public class TabViewModel : BindableBase
    {
        //public string Title => "111";
		private string title;

		public string Title
		{
			get { return title; }
			set { title = value; RaisePropertyChanged(); }
		}

	}
}
