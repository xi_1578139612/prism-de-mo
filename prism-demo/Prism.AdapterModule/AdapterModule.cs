﻿using Prism.AdapterModule.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prism.AdapterModule
{
    public class AdapterModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //containerProvider.Resolve<IRegionManager>().RegisterViewWithRegion<AdapterView>(RegionNames.AdapterRegion);
            var regionManage = containerProvider.Resolve<IRegionManager>();
            regionManage.RegisterViewWithRegion<AdapterView>(RegionNames.AdapterRegion);

            var region = regionManage.Regions[RegionNames.ItemControlRetion];
            var a = containerProvider.Resolve<ItemsView>();
            var b = containerProvider.Resolve<ItemsView>();
            var c = containerProvider.Resolve<ItemsView>();
            var d = containerProvider.Resolve<ItemsView>();
            region.Add(a);
            region.Add(b);
            region.Add(c);
            region.Add(d);


            var TabControl = regionManage.Regions[RegionNames.TabControlRetion];
            var e = containerProvider.Resolve<TabView>();
            var f = containerProvider.Resolve<TabView>();
            var g = containerProvider.Resolve<TabView>();
            
            TabControl.Add(e);
            TabControl.Add(f);
            TabControl.Add(g);

            e.SetTitle("国际");
            f.SetTitle("国内");

            var StackPanelControl = regionManage.Regions[RegionNames.StankPanelRetion];
            var h = containerProvider.Resolve<TabView>();
            var j = containerProvider.Resolve<TabView>();
            var k = containerProvider.Resolve<TabView>();

            StackPanelControl.Add(h);
            StackPanelControl.Add(j);
            StackPanelControl.Add(k);

            e.SetTitle("1111");
            f.SetTitle("222");

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            
        }
    }
}
