﻿using Prism.AdapterModule.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prism.AdapterModule.Views
{
    /// <summary>
    /// TabView.xaml 的交互逻辑
    /// </summary>
    public partial class TabView : UserControl
    {
        public TabView()
        {
            InitializeComponent();
        }

        //通知前端
        public void SetTitle(string title)
        {
            var vm = DataContext as TabViewModel;
            vm.Title = title;
        }
    }
}
